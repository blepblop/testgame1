﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    [SerializeField] private float shotsDelay = 20;
    [SerializeField] private float shotForce = 20;
    [SerializeField] private Transform muzzle;
    [SerializeField] private Rigidbody bullet;
    private float lastShotTime;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.timeSinceLevelLoad - shotsDelay < lastShotTime || !Input.GetMouseButton(0))
            return;

        var instance = GameObject.Instantiate(bullet, muzzle.position, Quaternion.identity);
        lastShotTime = Time.timeSinceLevelLoad;
        
        instance.AddForce(muzzle.forward * shotForce);
    }
}