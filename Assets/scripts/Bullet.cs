﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] private float damage = 25;
    [SerializeField] private float range = 6;
    private Vector3 startPosition;
    // Start is called before the first frame update
    void Start()
    {
        startPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if(Vector3.SqrMagnitude(startPosition - transform.position) > range * range)
            Destroy(gameObject);
    }

    private void OnCollisionEnter(Collision other)
    {
        var enemy = other.gameObject.GetComponent<Enemy>();

        if (enemy != null)
            enemy.Hit(damage);

        Destroy(gameObject);
    }
}